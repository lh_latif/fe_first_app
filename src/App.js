import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div >
      <header className="header">
        <h1>Web Blogger</h1>
        <div>
          <button className="btn1">Home</button>
        </div>
      </header>
      <div className="main-container">
        <main className="main">
          <h2>Ini Post</h2>
          <p>Ini konten post, silahkan dibaca</p>
        </main>
      </div>
    </div>
  );
}

export default App;
